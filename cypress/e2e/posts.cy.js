
/// <reference types="Cypress"/>

describe('template spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:8080');
    cy.get('#navPosts').click();
  })
  it('cancel', () => {
    cy.visit('http://localhost:8080');
    cy.get('#navPosts').click();
    cy.get('#30  > post-ui').click();
    cy.get('#cancel').click();
  })
  it('delete', () => {
    cy.visit('http://localhost:8080');
    cy.get('#navPosts').click();
    cy.get('#31  > post-ui');
    cy.get('#addbutton').click();
    cy.get('#delete').click();
  })
  it('update', () => {
    cy.visit('http://localhost:8080');
    cy.get('#navPosts').click();
    cy.get('#34  > post-ui').click();
    cy.get('#addbutton').click();
    cy.get('#title').type("Este titulo es mejor");
    cy.get('#update').click();
  })
})