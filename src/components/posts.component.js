import { LitElement, css, html } from "lit";
import { AllPostsUseCase } from "../usecases/all-posts.usecase";
import "./../ui/post.ui";


export class PostsComponent extends LitElement {
  
  static get properties() {
    return {
      posts: { type: Array },
      titleinput: {type: Object},
      contentinput: {type:Object},
      postIdselected: {type: Number}
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.posts = await AllPostsUseCase.execute();
    //preparing a copy from the original to use add and delete buttons
    this.myPostsList = this.posts;
    
    const addButton = this.querySelector("#addbutton");
    
    this.titleinput = document.querySelector('#title');
    this.contentinput = document.querySelector('#content');
    // Adding a default hidden style to buttons
   
    this.querySelector('#update').style.display = "none";
    this.querySelector('#delete').style.display = "none"

    addButton.addEventListener("click", () => {
      this.querySelector('#delete').style.display === "none" ? this.querySelector('#delete').style.display = "block" : this.querySelector('#delete').style.display ="none";
      this.querySelector('#update').style.display === "none" ? this.querySelector('#update').style.display = "block" : this.querySelector('#update').style.display ="none";
    });
    this.addEventListener('myPostsListDelete', (event) => {
      this.myPostsList = event.detail.myPostsList;
      this.requestUpdate();
    });
    this.addEventListener('myPostsListChange', (event) => {
      this.myPostsList = event.detail.myPostsList;
      this.requestUpdate();
    });
  }
  updateInput(title , content, id) {
    this.titleinput.value = title;
    this.contentinput.value = content;
    this.postIdselected = id;
  }; 
  render() {
    return html`
      <div class = "container">
          <div id="mypostscontainer">
              <button id="addbutton">Add</button>  
              <h1>Posts List</h1>
                  <ul id="titlelist">
                    ${this.myPostsList?.map(
                      (post,key) => html`<li id="${key}"   @click="${() => 
                                      this.updateInput(post.title, post.content, post.id)}">
                      <post-ui .post="${post}">${key}</post-ui>
                      </li>`
                    )}
                  </ul>
          </div>
          <one-post .postIdselected=${this.postIdselected}></one-post> 
      </div>     
    `;
  }
  createRenderRoot() {
    return this;
  }
}
customElements.define("genk-posts", PostsComponent);
