import { LitElement, css, html } from "lit";
import { AllPostsUseCase } from "../usecases/all-posts.usecase";
import "./../ui/post.ui";
import { DeletePostUseCase } from "../usecases/delete-post.usecase";


export class OnePostComponent extends LitElement {
  
 
  static get properties() {
    return {
      posts: { type: Array },
      inputref1: {type: Object},
      inputref2: {type:Object},
      postIdselected: {type: Number}
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.posts = await AllPostsUseCase.execute();
    //preparing a copy from the original to use add and delete buttons
    this.myPostsList = this.posts;
    
    const updateButton = document.querySelector("#update");
    const deleteButton = document.querySelector("#delete");
    const cancelButton = document.querySelector("#cancel");
    
    this.inputref1= document.querySelector('#title');
    this.inputref2 = document.querySelector('#content');


    cancelButton.addEventListener("click", ()=> {
      this.inputref1.value="";
      this.inputref2.value="";
    });

    deleteButton.addEventListener("click", () => {
      this.deletePost();
    });
    updateButton.addEventListener("click", () => {
      this.updatePost(this.inputref1.value, this.inputref2.value);
    });
  }
  
  async deletePost() {
    console.log("el identificador que hay que borrar es el: ", this.postIdselected)
    this.myPostsList = await DeletePostUseCase.execute(this.myPostsList,this.postIdselected);
    
    // Creating a custom event to communicate to posts.component
    this.dispatchEvent(new CustomEvent('myPostsListDelete', {
      detail: { myPostsList: this.myPostsList },
      bubbles: true,
      composed: true
    }));
    // We delete the inputs values
    const titledeleted = document.querySelector('#title');
    const contentdeleted = document.querySelector('#content');
    titledeleted.value = "";
    contentdeleted.value = "";
    
  }
  updatePost(title, content) {
    
    // We prepare the new values and look for the post to change
    const newPost = {id: this.postIdselected, title: title, content:content}
    const index = this.myPostsList.findIndex(post=> post.id === this.postIdselected)
    
    // If we find the post we change the values
    if (index !== -1){
      this.myPostsList[index] = newPost;
    }
    this.dispatchEvent(new CustomEvent('myPostsListChange', {
      detail: { myPostsList: this.myPostsList },
      bubbles: true,
      composed: true
    }));
    // We delete the inputs values
    const titleupdated = document.querySelector('#title');
    const contentupdated = document.querySelector('#content');
    titleupdated.value = "";
    contentupdated.value = "";
    this.requestUpdate();
  }
  
  render() {
    return html`
          <div id="myselectedpost">
            <h1>Posts Detail</h1>
            <div id="choice">
                <div id="field1">
                    <label for="title">Title</label>
                    <input name="title" type="text" id="title">
                </div>
                <div id="field2">
                    <label for="content">Content</label>
                    <input name="content" type="text" id="content">
                </div>
                <div id="field3">
                    <button id="cancel">Cancel</button> 
                    <button id="update">Update</button> 
                    <button id="delete">Delete</button>
                </div>
            </div>
          </div>      
    `;
  }
  
  createRenderRoot() {
    return this;
  }

}
customElements.define("one-post", OnePostComponent);
export default OnePostComponent;
